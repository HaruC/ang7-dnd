import { BrowserModule } from '@angular/platform-browser';
import { NgModule }      from '@angular/core';
import { DndComponent }  from './dnd.component';

@NgModule({
  declarations: [
    DndComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [DndComponent]
})
export class DndModule {
}

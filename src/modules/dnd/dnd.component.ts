import { ChangeDetectorRef, Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './dnd.component.html',
  styleUrls: ['./dnd.component.less']
})
export class DndComponent {
  public arrows = [];
  public transfer = [];

  public shapes = [
    {
      id: 'cube',
      points: '0 100,100 100,100 0,0 0',
    },
    {
      id: 'triangle',
      points: '0 100,50 0,100 100',
    }
  ];

  constructor(public cdr: ChangeDetectorRef) {}

  get getArrows() {
    this.drawArrows();
    return this.arrows;
  }

  set setArrows(item) {
    const index = this.arrows.length;
    this.arrows[index] = item;
  }

  get getTransfer() {
    return this.transfer;
  }

  set setTransfer(item) {
    const index = this.transfer.length;
    this.transfer[index] = item;
  }

  public log(...param) {
    return console.dir(param);
  }

  public drawArrows() {
    this.arrows = [];
    for (const item of this.transfer) {
      const start = this.countStart(item);
      const end = this.countEnd(item);
      end.forEach(dot => {
        const top = dot.top + 50;
        const left = dot.left + 50;
        const fin = `${left}, ${top}`;
        this.setArrows = `${start} ${fin}`;
      });
    }
  }

  public countStart(shape) {
    const elem = document.getElementById(shape.id).getBoundingClientRect();
    const top = elem.top + 50;
    const left = elem.left + 50;
    return `${left}, ${top}`;
  }

  public countEnd(shape) {
    const lsit = document.getElementsByClassName('clone');
    const elem = Array.from(lsit).filter((it) => it.id === shape.id);
    return elem.map((div => div.getBoundingClientRect()));
  }

  public onDragStart(ev) {
    ev.dataTransfer.setData('text', ev.target.id);
  }

  public onDragOver(ev) {
    ev.preventDefault();
  }

  public onDrop(ev) {
    ev.preventDefault();
    if (ev.target.className === 'dnd-to') {
      const data = ev.dataTransfer.getData('text');
      this.shapes.forEach((shape) => {
        if (shape.id === data) {
          this.setTransfer = shape;
        }
      });
    }
  }

  public proxy(ev) {
    return new Promise((res) => {
      this.onDrop(ev);
      return res();
    }).then(() => {
      this.cdr.detectChanges();
    });
  }
}
